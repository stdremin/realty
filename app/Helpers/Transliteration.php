<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 05.10.2018
 */

namespace App\Helpers;


class Transliteration
{
	/** @var array  */
	public static $converter = array(
		'а' => 'a', 'б' => 'b', 'в' => 'v',
		'г' => 'g', 'д' => 'd', 'е' => 'e',
		'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
		'и' => 'i', 'й' => 'y', 'к' => 'k',
		'л' => 'l', 'м' => 'm', 'н' => 'n',
		'о' => 'o', 'п' => 'p', 'р' => 'r',
		'с' => 's', 'т' => 't', 'у' => 'u',
		'ф' => 'f', 'х' => 'h', 'ц' => 'c',
		'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch',
		'ь' => '\'', 'ы' => 'y', 'ъ' => '\'',
		'э' => 'e', 'ю' => 'yu', 'я' => 'ya',

		'А' => 'A', 'Б' => 'B', 'В' => 'V',
		'Г' => 'G', 'Д' => 'D', 'Е' => 'E',
		'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z',
		'И' => 'I', 'Й' => 'Y', 'К' => 'K',
		'Л' => 'L', 'М' => 'M', 'Н' => 'N',
		'О' => 'O', 'П' => 'P', 'Р' => 'R',
		'С' => 'S', 'Т' => 'T', 'У' => 'U',
		'Ф' => 'F', 'Х' => 'H', 'Ц' => 'C',
		'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch',
		'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'',
		'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
	);

	/** @var array  */
	public static $defaultOptions = [
		'change_case' => 'L',
		'replace_other' => '_',
	];

	/**
	 * @method translit
	 * @param string $value
	 * @param array $options
	 *
	 * @return string
	 */
	public static function translit(string $value = '', $options = [])
	{
		$options = array_merge(static::$defaultOptions, $options);

		$str = strtr($value, static::$converter);

		switch ($options['change_case']){
			case 'L':
				$str = strtolower($str);
				break;
			case 'U':
				$str = strtoupper($str);
				break;
		}

		$str = preg_replace('~[^-a-z0-9_]+~u', static::$defaultOptions['replace_other'], $str);
		$result = trim($str, "-");

		return $result;
	}

}