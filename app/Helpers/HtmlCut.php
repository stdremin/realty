<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 26.10.2018
 */

namespace App\Helpers;


class HtmlCut
{
	/**
	 * @method cut
	 * @param $html
	 * @param $size
	 *
	 * @return string
	 */
	public static function cut($html, $size)
	{
		$symbols = strip_tags($html);
		$symbols_len = strlen($symbols);

		if($symbols_len < strlen($html))
		{
			$strip_text = static::strip_words($html, $size);

			if($symbols_len > $size)
				$strip_text = $strip_text."...";

			$final_text = static::closetags($strip_text);
		}
		elseif($symbols_len > $size)
			$final_text = substr($html, 0, $size)."...";
		else
			$final_text = $html;

		return $final_text;
	}

	/**
	 * @method strip_words
	 * @param $string
	 * @param $count
	 *
	 * @return string
	 */
	protected static function strip_words($string, $count)
	{
		$splice_pos = null;

		$ar = preg_split("/(<.*?>|\\s+)/s", $string, -1, PREG_SPLIT_DELIM_CAPTURE);
		foreach($ar as $i => $s)
		{
			if(substr($s, 0, 1) != "<")
			{
				$count -= strlen($s);
				if($count <= 0)
				{
					$splice_pos = $i;
					break;
				}
			}
		}

		if(isset($splice_pos))
		{
			array_splice($ar, $splice_pos+1);
			return implode('', $ar);
		}
		else
		{
			return $string;
		}
	}

	/**
	 * @method closeTags
	 * @param $html
	 *
	 * @return string
	 */
	protected static function closeTags($html)
	{
		preg_match_all("#<([a-z0-9]+)([^>]*)(?<!/)>#iu", $html, $result);
		$openedtags = array_map("strtolower", $result[1]);

		preg_match_all("#</([a-z0-9]+)>#iu", $html, $result);
		$closedtags = array_map("strtolower", $result[1]);

		$len_opened = count($openedtags);

		if(count($closedtags) == $len_opened)
		{
			return $html;
		}

		$openedtags = array_reverse($openedtags);

		static $tagsWithoutClose = array('input'=>1, 'img'=>1, 'br'=>1, 'hr'=>1, 'meta'=>1, 'area'=>1, 'base'=>1, 'col'=>1, 'embed'=>1, 'keygen'=>1, 'link'=>1, 'param'=>1, 'source'=>1, 'track'=>1, 'wbr'=>1);

		for($i = 0; $i < $len_opened; $i++)
		{
			if(isset($tagsWithoutClose[$openedtags[$i]]))
			{
				continue;
			}
			if(!in_array($openedtags[$i], $closedtags))
			{
				$html .= '</'.$openedtags[$i].'>';
			}
			else
			{
				unset($closedtags[array_search($openedtags[$i], $closedtags)]);
			}
		}

		return $html;
	}
}