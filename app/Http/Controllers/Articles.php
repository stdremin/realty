<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 26.10.2018
 */

namespace App\Http\Controllers;


use App\Models\ArticlesModel;

class Articles extends Controller
{
	/**
	 * @method index
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$items = ArticlesModel::select(['*'])->paginate(9);

		return view('pages.articles', [
			'items' => $items,
			'title' => 'Статьи о недвижимости',
			'subTitle' => ''
		]);
	}

	/**
	 * @method getDetail
	 * @param $code
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function getDetail($code)
	{
		$element = ArticlesModel::whereCode($code)->first();

		$moreItems = ArticlesModel::extremeArticle();

		return view('pages.article_detail', [
			'element' => $element,
			'title' => $element->title,
			'subTitle' => '',
			'moreItems' => $moreItems
		]);
	}

	public function deals()
	{
		$items = ArticlesModel::select(['*'])->paginate(9);

		return view('parts.index.news', [
			'moreItems' => $items,
		]);
	}
}
