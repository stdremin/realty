<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 18.10.2018
 */

namespace App\Http\Controllers;


use App\Models\ArticlesModel;

class IndexController extends Controller
{
	public function indexPage()
	{
		$moreItems = ArticlesModel::extremeArticle();

		return view('content.index_content', ['moreItems' => $moreItems]);
	}

}
