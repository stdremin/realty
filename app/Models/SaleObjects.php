<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SaleObjects
 *
 * @property int $id
 * @property string $title
 * @property string|null $code
 * @property int|null $preview_image
 * @property int|null $detail_image
 * @property string|null $preview_text
 * @property string|null $detail_text
 * @property string|null $map_point_id
 * @property string|null $section_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereDetailImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereDetailText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereMapPointId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects wherePreviewImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SaleObjects whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SaleObjects extends Model
{
    protected $table = 'sale_objects';
    protected $fillable = [
    	'title', 'code', 'preview_image', 'detail_image', 'preview_text', 'detail_text',
	    'map_point_id', 'section_id'
    ];
}
