<?php

namespace App\Models;

use App\Helpers\HtmlCut;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * App\Models\ArticlesModel
 *
 * @property int $id
 * @property string $title
 * @property string|null $code
 * @property string|null $preview_image
 * @property string|null $detail_image
 * @property string|null $preview_text
 * @property string|null $detail_text
 * @property int|null $section_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereDetailImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereDetailText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel wherePreviewImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel wherePreviewText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereSectionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ArticlesModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ArticlesModel extends Model
{
    protected $table = 'articles';

    protected $fillable = [
	    'title', 'code', 'preview_image', 'detail_image', 'preview_text', 'detail_text', 'section_id'
    ];

	/**
	 * @method getPreviewImageAttribute
	 * @param $src
	 *
	 * @return mixed
	 */
    public function getPreviewImageAttribute($src)
    {
    	return Storage::disk('admin')->url($src);
    }

	/**
	 * @method getDetailImageAttribute
	 * @param $src
	 *
	 * @return mixed
	 */
	public function getDetailImageAttribute($src)
	{
		return Storage::disk('admin')->url($src);
	}

	/**
	 * @method getPreviewTextAttribute
	 * @param $text
	 *
	 * @return string
	 */
	public function getPreviewTextAttribute($text)
	{
		return HtmlCut::cut($text, 200);
	}

	/**
	 * @method extremeArticle
	 * @param array $filter
	 * @param array $order
	 *
	 * @return ArticlesModel[]|\Illuminate\Database\Eloquent\Collection
	 */
	public static function extremeArticle($filter = [], $order = [])
	{
		return static::select(['id','code', 'title', 'preview_image', 'preview_text'])
			->orderBy('id', 'desc')
			->limit(3)
			->get();
	}


	/**
	 * @method detailUrl
	 * @return string
	 */
	public function detailUrl()
	{
		return route('detail_article', ['code' => $this->code]);
	}
}
