<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 26.10.2018
 */

namespace App\FakerGenerator;


use App\Helpers\Transliteration;
use Faker\Factory;
use Faker\Generator;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class ArticleGenerator
{
	/** @var Configurating */
	protected $config;

	/** @var Collection */
	protected $photos;

	/** @var Collection */
	protected $text;

	/** @var Model */
	protected $model;

	protected $count = 20;

	public function __construct($count = 20)
	{
		$this->count = $count ? : 20;
		$this->config = (new Configurating())->getArticleConfig();
	}

	/**
	 * @method getRandomFlickrPhoto
	 * @param string $tags
	 *
	 * @return Collection
	 */
	public function getRandomFlickrPhoto($tags = '')
	{
		$tags = explode(",", $tags);
		$tags = array_filter($tags);
		foreach ($tags as $k => $t)
			$tags[$k] = rawurlencode($t);
		$tagsParam = implode(",", $tags);
		$conf = $this->config['flikr'];
		$flickrApiUrl = $conf['url'];

		$client = new Client();

		$paramsApi = [
			'method' => 'flickr.photos.search',
			'api_key' => $conf['key'],
			'tags' => $tagsParam,
			'tag_mode' => 'all',
			'license' => 1,
			'per_page' => $this->count,
			'privacy_filter' => 1,
			'safe_search' => 1,
			'content_type' => 1,
			'extras' => 'url_z,url_o,url_l',
			'sort' => 'interestingness-desc',
			'page' => rand(1, 30),
			'format' => 'php_serial',
		];

		$uri = new Uri($flickrApiUrl);

		$response = $client->get($uri->withQuery(http_build_query($paramsApi)));
		$flickrData = unserialize($response->getBody()->getContents());
		$returnPhotos = array();
		if ($flickrData["stat"] == "ok"){
			// http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{o-secret}_o.(jpg|gif|png)
			// http://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
			/*
			s	small square 75x75
			q	large square 150x150
			t	thumbnail, 100 on longest side
			m	small, 240 on longest side
			n	small, 320 on longest side
			-	medium, 500 on longest side
			z	medium 640, 640 on longest side
			c	medium 800, 800 on longest side?
			b	large, 1024 on longest side*
			o	original image, either a jpg, gif or png, depending on source format
			*/
			foreach ($flickrData["photos"]["photo"] as $photo) {
				if (!empty($photo["url_l"])){
					$returnPhotos[] = array(
						"small" => $photo["url_z"],
						"big" => $photo["url_l"],
					);
				}
			}
		}
		$this->photos = collect($returnPhotos);

		return $this->photos;
	}

	/**
	 * @method randomText
	 * @param string $themeCode
	 *
	 * @return array
	 */
	protected function randomText($themeCode = '')
	{
		$arResult = [];

		$theme = $this->config['yandex']['links'][$themeCode] ??
			collect($this->config['yandex']['links'])->random();

		$client = new Client();
		$html = $client->get($theme)->getBody()->getContents();

		$html = preg_replace('#<div>.*<\/div>#i', '', $html);

		preg_match('#<strong>Тема:\s+«(.*)»<\/strong>#', $html, $matchTitle);

		$arResult['text'] = preg_replace('#<strong>.*<\/strong>#', '', $html);
		$arResult['title'] = $matchTitle[1];
		$arResult['code'] = Transliteration::translit($arResult['title']);

		return $arResult;
	}

	public function generateText($theme = '')
	{
		$text = [];
		for ($i = 1; $i <= $this->count; $i++){
			$text[] = $this->randomText($theme);
		}

		$this->text = collect($text);

		return $this->text;
	}

	/**
	 * @method getModel - get param model
	 * @return Model
	 */
	public function getModel()
	{
		return $this->model;
	}

	/**
	 * @method setModel - set param Model
	 * @param Model $model
	 */
	public function setModel($model)
	{
		$this->model = $model;
	}

	/**
	 * @method getPhotos - get param photos
	 * @return Collection
	 */
	public function getPhotos()
	{
		return $this->photos;
	}

}
