<?php
/**
 * Created by OOO 1C-SOFT.
 * User: GrandMaster
 * Date: 27.10.18
 */

namespace App\Admin\Controllers;


use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use App\Models;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ArticlesController extends Controller
{
	use HasResourceActions;

	/**
	 * @method grid
	 * @return Grid
	 */
	protected function grid()
	{
		$grid = new Grid(new Models\ArticlesModel());

		$grid->id('ID')->sortable();
		$grid->created_at('Создано');
		$grid->updated_at('Обновлено');
		$grid->title('Название');
//		$grid->preview_image('Малая фотка');
//		$grid->detail_image('Большая фотка');

		return $grid;
	}

	/**
	 * @method index
	 * @param Content $content
	 *
	 * @return Content
	 */
	public function index(Content $content)
	{
		return $content
			->header('Статьи')
			->description(' ')
			->body($this->grid());
	}

	/**
	 * @method form
	 * @return Form
	 */
	protected function form()
	{
		$form = new Form(new Models\ArticlesModel());

		$form->tab('Основное', function (Form $form){
			$form->display('id', 'ID');
			$form->display('created_at', 'Создано');
			$form->display('updated_at', 'Обновлено');
			$form->text('title', 'Название');
		})->tab('Описание', function (Form $form){
			$form->editor('preview_text', 'Краткое описание');
			$form->editor('detail_text', 'Детальное описание');
		})->tab('Фотки', function (Form $form){
			$form->image('preview_image', 'Малая фотка')->uniqueName();
			$form->image('detail_image', 'Большая фотка')->uniqueName();
		});

		return $form;
	}

	/**
	 * @method show
	 * @param $id
	 *
	 * @return Show
	 */
	protected function show($id)
	{
		$show = new Show(Models\ArticlesModel::findOrFail($id));

		$show->id('ID')->sortable();
		$show->created_at('Создано');
		$show->updated_at('Обновлено');
		$show->title('Название');
		$show->field('preview_image', 'Малая фотка')->image();
		$show->field('detail_image', 'Большая фотка')->image();

		return $show;
	}

	/**
	 * @method edit
	 * @param $id
	 * @param Content $content
	 *
	 * @return Content
	 */
	public function edit($id, Content $content)
	{
		return $content
			->header('Сохранение')
			->description(' ')
			->body($this->form()->edit($id));
	}
}
