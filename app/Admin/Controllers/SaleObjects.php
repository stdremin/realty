<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 26.10.2018
 */

namespace App\Admin\Controllers;


use App\FakerGenerator\ArticleGenerator;
use App\Http\Controllers\Controller;
use App\Models;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;
use Encore\Admin\Widgets\Tab;
use Illuminate\Foundation\Application;

class SaleObjects extends Controller
{
	use HasResourceActions;

	public function index(Content $content)
	{
		return $content
			->header('Объекты на продажу')
			->description(' ')
			->body($this->grid());
	}

	protected function grid()
	{
		$grid = new Grid(new Models\SaleObjects());

		$grid->id('ID')->sortable();
		$grid->created_at('Создано');
		$grid->updated_at('Обновлено');
		$grid->title('Название');
		$grid->preview_image('Малая фотка');
		$grid->detail_image('Большая фотка');
		$grid->map_point_id('Точка на карте');
		$grid->section_id('Категория');

		return $grid;
	}

	public function create(Content $content)
	{
		return $content
			->header('Create')
			->description('description')
			->body($this->form());
	}

	protected function form()
	{
		$form = new Form(new Models\SaleObjects());

		$form->tab('Основное', function (Form $form){
			$form->display('id', 'ID');
			$form->display('created_at', 'Создано');
			$form->display('updated_at', 'Обновлено');
			$form->text('title', 'Название');
		})->tab('Описание', function (Form $form){
			$form->editor('preview_text', 'Краткое описание');
			$form->editor('detail_text', 'Детальное описание');
		})->tab('Фотки', function (Form $form){
			$form->image('preview_image', 'Малая фотка')->uniqueName();
			$form->image('detail_image', 'Большая фотка')->uniqueName();
		});

		$faker = new ArticleGenerator();
		$faker->getRandomFlickrPhoto();

		return $form;
	}

	protected function show($id)
	{
		$show = new Show(Models\SaleObjects::findOrFail($id));

		$show->id('ID')->sortable();
		$show->created_at('Создано');
		$show->updated_at('Обновлено');
		$show->title('Название');
		$show->field('preview_image', 'Малая фотка')->image();
		$show->field('detail_image', 'Большая фотка')->image();
//		$show->map_point_id('Точка на карте');
//		$show->section_id('Категория');

		return $show;
	}

	/**
	 * Edit interface.
	 *
	 * @param mixed   $id
	 * @param Content $content
	 * @return Content
	 */
	public function edit($id, Content $content)
	{
		return $content
			->header('Сохранение')
			->description(' ')
			->body($this->form()->edit($id));
	}
}
