<?php

use Illuminate\Routing\Router;
use App\Admin\Controllers\SaleObjects;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
//    $router->resource('/objects/items', 'SaleObjects');
	$router->resource('/articles', 'ArticlesController');

});
