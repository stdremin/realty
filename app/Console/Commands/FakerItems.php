<?php

namespace App\Console\Commands;

use App\FakerGenerator\ArticleGenerator;
use App\Models\ArticlesModel;
use App\Models\SaleObjects;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class FakerItems extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'faker:text';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Генерация текста и фоток';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{

		$ArticleGenerator = new ArticleGenerator(27);
		$txt = $ArticleGenerator->generateText();

		$photos = $ArticleGenerator->getRandomFlickrPhoto();

		//'title', 'code', 'preview_image', 'detail_image', 'preview_text', 'detail_text',
		//	    'map_point_id', 'section_id'
		foreach ($txt as $k => $item) {
			sleep(3);
			$photo = $photos->get($k);
			$smallName = $this->photoName($photo["small"]);
			Storage::disk('admin')->put('/images/articles/'.$smallName, file_get_contents($photo["small"]));

			$bigName = $this->photoName($photo["big"]);
			Storage::disk('admin')->put('/images/articles/'.$bigName, file_get_contents($photo["big"]));

			ArticlesModel::create([
				'title' => $item['title'],
				'code' => $item['code'],
				'preview_image' => '/images/articles/'.$smallName,
				'detail_image' => '/images/articles/'.$bigName,
				'preview_text' => $item['text'],
				'detail_text' => $item['text']
			]);
		}
	}

	protected function photoName($path)
	{
		$tmp = explode('/', $path);

		return array_pop($tmp);
	}
}
