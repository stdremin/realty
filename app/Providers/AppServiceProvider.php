<?php

namespace App\Providers;

use App\Helpers\Tools;
use Illuminate\Support\ServiceProvider;
use App\FakerGenerator;
use Encore\Admin\Config\Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    Tools::varDumperHandler();
//	    Config::load();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
//	    $this->app->singleton(FakerGenerator\Configurating::class, new FakerGenerator\Configurating());
//	    $this->app->bind(FakerGenerator\ArticleGenerator::class, function ($app) {
//		    $article = new FakerGenerator\ArticleGenerator($app->get(FakerGenerator\Configurating::class));
//
//		    return $article;
//	    });
    }
}
