<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\FakerGenerator;

class FakerGeneratorProvider extends ServiceProvider
{
	protected $defer = true;

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton(FakerGenerator\Configurating::class, new FakerGenerator\Configurating());
		$this->app->bind(FakerGenerator\ArticleGenerator::class, function ($app) {
			$article = new FakerGenerator\ArticleGenerator($app->get(FakerGenerator\Configurating::class));

			return $article;
		});
	}

	public function provides()
	{
		return [
			FakerGenerator\ArticleGenerator::class
		];
	}
}
