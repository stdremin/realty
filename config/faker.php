<?php
/**
 * Created by OOO 1C-SOFT.
 * User: Dremin_S
 * Date: 26.10.2018
 */

return [
	'yandex' => [
		'links' => [
			"astronomy" => "https://yandex.ru/referats/write/?t=astronomy",
			"geology" => "https://yandex.ru/referats/write/?t=geology",
			"gyroscope" => "https://yandex.ru/referats/write/?t=gyroscope",
			"literature" => "https://yandex.ru/referats/write/?t=literature",
			"marketing" => "https://yandex.ru/referats/write/?t=marketing",
			"mathematics" => "https://yandex.ru/referats/write/?t=mathematics",
			"music" => "https://yandex.ru/referats/write/?t=music",
			"polit" => "https://yandex.ru/referats/write/?t=polit",
			"agrobiologia" => "https://yandex.ru/referats/write/?t=agrobiologia",
			"law" => "https://yandex.ru/referats/write/?t=law",
			"psychology" => "https://yandex.ru/referats/write/?t=psychology",
			"geography" => "https://yandex.ru/referats/write/?t=geography",
			"physics" => "https://yandex.ru/referats/write/?t=physics",
			"philosophy" => "https://yandex.ru/referats/write/?t=philosophy",
			"chemistry" => "https://yandex.ru/referats/write/?t=chemistry",
			"estetica" => "https://yandex.ru/referats/write/?t=estetica",
		],
		'translate_key' => 'trnsl.1.1.20160909T083559Z.e677138108470233.7d6c343b44fdb4f6d8cb481c6b4589814cf18c46',
	],
	'flikr' => [
		'url' => 'https://api.flickr.com/services/rest/',
		'key' => '09a0ea92182a4f5d5c56b80e0b634710'
	],
];
