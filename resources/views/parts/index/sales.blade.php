<section class="section-wrap bg-color-overlay--2" style="background-image: url(http://realty.io/img/newsletter/newsletter.jpg);">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="title-row">
                    <h2 class="section-title">Продажа квартир в ипотеке</h2>
                </div>

                <div class="row">
                    <div class="testimonial col-md-7">
                        <div class="testimonial__info">
                            <span class="testimonial__author">Процедура продажи</span>
                        </div>
                        <div class="testimonial__body">
                            @include('parts.index.time-line')
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form_sale">
                            @include('parts.index_form')
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
