<section class="section-wrap bg-color">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="title-row">
                    <h2 class="section-title">Отзывы клиентов</h2>
                    <p class="subtitle">Те, кто уже пробовал.</p>
                </div>

                <div id="owl-testimonials" class="owl-carousel owl-theme owl-carousel--arrows-outside">

                    <div class="testimonial clearfix">
                        <img href="{{ asset('img/testimonials/1.png') }}" alt="" class="testimonial__img">
                        <div class="testimonial__info">
                            <span class="testimonial__author">Гадя Хренова</span>
                            <span class="testimonial__company">DeoThemes</span>
                        </div>
                        <div class="testimonial__body">
                            <p class="testimonial__text">“Потерялася я, но меня нашли)) Я так счастлива!!!.”</p>
                            <div class="testimonial__rating">
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial clearfix">
                        <img href="{{ asset('img/testimonials/2.png') }}" alt="" class="testimonial__img">
                        <div class="testimonial__info">
                            <span class="testimonial__author">Василий Тапочкин</span>
                            <span class="testimonial__company">DeoThemes</span>
                        </div>
                        <div class="testimonial__body">
                            <p class="testimonial__text">“Если бы не эти ребята, я не знаю что со мной бы было.”</p>
                            <div class="testimonial__rating">
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial clearfix">
                        <img href="{{ asset('img/testimonials/1.png')}}" alt="" class="testimonial__img">
                        <div class="testimonial__info">
                            <span class="testimonial__author">Роберт Пупкин</span>
                            <span class="testimonial__company">DeoThemes</span>
                        </div>
                        <div class="testimonial__body">
                            <p class="testimonial__text">“Как мы уже знаем, громкостнoй прогрессийный период неравномерен. Глиссандо просветляет
                                флэнжер. Соинтервалие, и это особенно заметно у Чарли Паркера или Джона Колтрейна, изящно продолжает тон-полутоновый
                                хамбакер, не говоря уже о том, что рок-н-ролл мертв.”</p>
                            <div class="testimonial__rating">
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                            </div>
                        </div>
                    </div>

                    <div class="testimonial clearfix">
                        <img href="{{ asset('img/testimonials/2.png') }}" alt="" class="testimonial__img">
                        <div class="testimonial__info">
                            <span class="testimonial__author">Alexander Samokhin</span>
                            <span class="testimonial__company">DeoThemes</span>
                        </div>
                        <div class="testimonial__body">
                            <p class="testimonial__text">“Every detail has been taken care these team are realy amazing and talented! I will work only
                                to help your sales goals.”</p>
                            <div class="testimonial__rating">
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                                <i class="ui-star"></i>
                            </div>
                        </div>
                    </div>

                </div> <!-- end owl-carousel -->
            </div>
        </div>
    </div>
</section>