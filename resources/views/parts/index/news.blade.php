<!-- From Blog -->
<section class="section-wrap" style="margin-bottom: 0; padding-bottom: 0; padding-top: 0">
    <div class="container">
        <div class="title-row title-row--boxed text-center">
            <h2 class="section-title">Сделки</h2>
        </div>
        <div class="row card-row">

            @foreach($moreItems as $item)
                <?/** @var \App\Models\ArticlesModel $item */?>
                <div class="col-lg-4">
                <article class="entry card box-shadow hover-up">
                    <div class="entry__img-holder card__img-holder">
                        <a href="{{ $item->detailUrl() }}">
                            <img src="{{ $item->preview_image }}" class="entry__img" alt="{{ $item->title }}">
                        </a>
                        <div class="entry__date">
                            <span class="entry__date-day">27</span>
                            <span class="entry__date-month">авг.</span>
                        </div>
                        <div class="entry__body card__body">
                            <h4 class="entry__title">
                                <a href="{{ $item->detailUrl() }}">{{ $item->title }}</a>
                            </h4>
                            <ul class="entry__meta">
                                <li class="entry__meta-category">
                                    <i class="ui-category"></i>
                                    <a href="#">Marketing</a>
                                </li>
                                <li class="entry__meta-comments">
                                    <i class="ui-comments"></i>
                                    <a href="#">3 Comments</a>
                                </li>
                            </ul>
                            <div class="entry__excerpt">
                                {!! $item->preview_text !!}
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            @endforeach

        </div>
    </div>
</section> <!-- end from blog -->
