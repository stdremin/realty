<section class="section-wrap section-wrap--pb-large bg-gradient" style="background-image: url({{ asset('img/partners/map.png') }});">
    <div class="container">
        <div class="title-row title-row--boxed text-center">
            <h2 class="section-title">Карта наших объкетов</h2>
            <p class="subtitle">Здесь будет гугл карта с объектами</p>
        </div>
        <div class="row justify-content-center text-center">
            <div class="col-lg-10">
                <div class="row pb-48">
                    <div class="col-md col-sm-6">
                        <img src="{{ asset('img/partners/1.png') }}" alt="">
                    </div>
                    <div class="col-md col-sm-6">
                        <img src="{{ asset('img/partners/2.png') }}" alt="">
                    </div>
                    <div class="col-md col-sm-6">
                        <img src="{{ asset('img/partners/3.png') }}" alt="">
                    </div>
                    <div class="col-md col-sm-6">
                        <img src="{{ asset('img/partners/4.png') }}" alt="">
                    </div>
                    <div class="col-md col-sm-6">
                        <img src="{{ asset('img/partners/5.png') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>