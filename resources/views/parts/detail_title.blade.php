<!-- Page Title -->
<section class="page-title blog-featured-img bg-color-overlay bg-color-overlay--1 text-center" style="background-image: url({{ $bgImage }});">
    <div class="container">
        <div class="page-title__holder">
            <h1 class="page-title__title">{{ $title }}</h1>
            <ul class="entry__meta">
                {{--<li class="entry__meta-author">
                    <a href="#" class="entry__meta-author-url">
                        <img src="img/blog/author.png" class="entry__meta-author-img" alt="">
                        <span>by DeoThemes</span>
                    </a>
                </li>--}}
                <li class="entry__meta-date">
                    Апрель 13, 2018
                </li>
            </ul>
        </div>
    </div>
    <div class="blur-2"></div>
</section> <!-- end page title -->