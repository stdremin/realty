<div class="row justify-content-center">
    <div class="col-lg-10">
        <!-- Optin Form -->
        <div class="optin">
            <h3 class="optin__title">Оставить заявку</h3>
            <form class="optin__form">
                <div class="optin__form-group form-group">
                    <input type="text" class="form-input optin__input" id="optin-name" required>
                    <label for="optin-name" class="optin__label">Как вас зовут</label>
                    <span class="input-underline"></span>
                </div>
                <div class="optin__form-group form-group">
                    <input type="email" class="form-input optin__input" id="optin-email" required>
                    <label for="optin-email" class="optin__label">Ваш e-mail</label>
                    <span class="input-underline"></span>
                </div>

                <button class="optin__btn btn btn--md btn--color btn--button">Отправить</button>
            </form>
        </div>
    </div>
</div>