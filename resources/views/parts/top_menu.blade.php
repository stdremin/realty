<header class="nav">
    <div class="nav__holder nav--sticky">
        <div class="container-fluid container-semi-fluid nav__container">
            <div class="flex-parent">

                <div class="nav__header">
                    <!-- Logo -->
                    <a href="/" class="logo-container flex-child">
                        <img class="logo" href="{{ asset('img/logo.png" srcset="img/logo.png 1x, img/logo@2x.png 2x" alt="logo') }}" />
                    </a>

                    <!-- Mobile toggle -->
                    <button type="button" class="nav__icon-toggle" id="nav__icon-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="nav__icon-toggle-bar"></span>
                        <span class="nav__icon-toggle-bar"></span>
                        <span class="nav__icon-toggle-bar"></span>
                    </button>
                </div>

                <!-- Navbar -->
                <nav id="navbar-collapse" class="nav__wrap collapse navbar-collapse">
                    <ul class="nav__menu">
                        <li class="active">
                            <a href="/" class="anchor_link main">Главная</a>
                        </li>
                        {{--<li class="nav__dropdown">
                            <a href="#">Pages</a>
                            <i class="ui-arrow-down nav__dropdown-trigger"></i>
                            <ul class="nav__dropdown-menu">
                                <li><a href="about.html">About</a></li>
                                <li><a href="services.html">Services</a></li>
                                <li><a href="pricing.html">Pricing</a></li>
                                <li><a href="privacy-policy.html">Privacy Policy</a></li>
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </li>--}}
                        <li class="nav__dropdown">
                            <a href="/#sale_block" class="anchor_link">Продажа</a>
                        </li>
                        <li class="nav__dropdown">
                            <a href="/#purchase_block" class="anchor_link">Покупка</a>
                        </li>
                        <li class="nav__dropdown">
                            <a href="/articles">Статьи</a>
                            {{--<i class="ui-arrow-down nav__dropdown-trigger"></i>
                            <ul class="nav__dropdown-menu">
                                <li><a href="icons.html">Icons</a></li>
                                <li><a href="elements.html">Elements</a></li>
                            </ul>--}}
                        </li>
                        <li>
                            <a href="/about">О нас</a>
                        </li>
                        <li>
                            <a href="/contacts">Контактная информация</a>
                        </li>
                    </ul> <!-- end menu -->
                </nav> <!-- end nav-wrap -->

                <div class="nav__btn-holder nav--align-right">
                    <a href="#" class="btn nav__btn">
                        <span class="nav__btn-text">Телефон</span>
                        <span class="nav__btn-phone">+7 (925) 515 33 24</span>
                    </a>
                </div>

            </div> <!-- end flex-parent -->
        </div> <!-- end container -->

    </div>
</header>
