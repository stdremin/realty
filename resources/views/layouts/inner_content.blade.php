@extends('layouts.base')
@section('main_content')

    @yield('inner_title')
    @yield('detail_title')
    @yield('inner_text')

    <!-- CTA -->
    <section class="section-wrap newsletter bg-color-overlay bg-color-overlay--2 inner_content" style="background-image: url({{ asset('img/newsletter/newsletter.jpg') }});">
        <div class="container">
            <div class="title-row text-center">
                <h3 class="section-title">Какой-то мотивирующий заголовок</h3>
                <p class="subtitle">Подзаголовок</p>
                <p><a href="#" class="btn btn--lg btn--color">
                    <span>Давайте работать вместе!</span>
                    </a></p>
            </div>
        </div>
    </section>
    <!-- end cta -->

@stop