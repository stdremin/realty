<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>купить квартиру ниже рынка по стоимости</title>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/font-icons.css') }}" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('img/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('img/apple-touch-icon-114x114.png') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.css" />

    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
</head>
<body>

<!-- Preloader -->
<div class="loader-mask">
    <div class="loader">"Загрузка..."</div>
</div>

<main class="main-wrapper">

    <!-- Navigation -->
    @include('parts.top_menu')
    <!-- end navigation -->

    @stack('svg_main_img')

    <div class="content-wrapper oh">

        @yield('main_content')

        <!-- Footer -->
        <footer class="footer">
            <div class="container">
                <div class="footer__widgets">
                    <div class="row">

                        <div class="col-lg-3 col-md-6">
                            <div class="widget widget-about-us">
                                <!-- Logo -->
                                <a href="index.html" class="logo-container flex-child">
                                    <img class="logo" src="{{ asset('img/logo.png') }}" srcset="img/logo.png 1x, img/logo@2x.png 2x" alt="logo">
                                </a>
                                <p class="mt-24 mb-32">Мы в соц. сетях.</p>
                                <div class="socials">
                                    <a href="#" class="social social-twitter" aria-label="twitter" title="twitter" target="_blank"><i class="ui-twitter"></i></a>
                                    <a href="#" class="social social-facebook" aria-label="facebook" title="facebook" target="_blank"><i class="ui-facebook"></i></a>
                                    <a href="#" class="social social-google-plus" aria-label="google plus" title="google plus" target="_blank"><i class="ui-google"></i></a>
                                </div>
                            </div>
                        </div> <!-- end about us -->


                        <div class="col-lg-2 offset-lg-3 col-md-6">
                            <div class="widget widget_nav_menu">
                                <h5 class="widget-title">Ресурсы</h5>
                                <ul>
                                    <li><a href="#">О нас</a></li>
                                    <li><a href="#">Объекты продажи</a></li>
                                    <li><a href="#">Цены</a></li>
                                    <li><a href="#">Новости</a></li>
                                    <li><a href="#">Контакты</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6">
                            <div class="widget widget_nav_menu">
                                <h5 class="widget-title">Решения</h5>
                                <ul>
                                    <li><a href="#">Marketing Strategy</a></li>
                                    <li><a href="#">Advertising</a></li>
                                    <li><a href="#">SMM</a></li>
                                    <li><a href="#">SEO</a></li>
                                    <li><a href="#">Google AdWords</a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-6">
                            <div class="widget widget-address">
                                <h5 class="widget-title">Компания</h5>
                                <ul>
                                    <li>
                                        <address>Москва, ул. Красивая, д.56</address>
                                    </li>
                                    <li>
                                        <span>Телефон: </span>
                                        <a href="tel:+1-800-1554-456-123">+ 7 (800) 155 4561</a>
                                    </li>
                                    <li>
                                        <span>Email: </span>
                                        <a href="mailto:info@realty.ru">info@realty.ru</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div> <!-- end container -->

            <div class="footer__bottom top-divider">
                <div class="container text-center">
            <span class="copyright">
              &copy; 2018 Margin, все права защищены{{--, Made by <a href="https://deothemes.com">DeoThemes</a>--}}
            </span>
                </div>
            </div> <!-- end footer bottom -->
        </footer> <!-- end footer -->

        <div id="back-to-top">
            <a href="#top"><i class="ui-arrow-up"></i></a>
        </div>

    </div> <!-- end content wrapper -->
</main> <!-- end main wrapper -->


<!-- jQuery Scripts -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/plugins.js') }}"></script>

@stack('scripts')
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.2/dist/jquery.fancybox.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
<script src="{{ asset('js/scripts.js') }}"></script>

{{--<script src="{{ asset('js/cookies.js') }}"></script>--}}
</body>
</html>
