@extends('layouts.base')
@section('main_content')
    <!-- Hero -->
    <section class="hero">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 offset-lg-1">
                    <div class="hero__text-holder">
                        <h1 class="hero__title hero__title--boxed">Cрочный выкуп квартир</h1>
                        <h2 class="hero__subtitle">
                            <span class="highlight">от 3 до 7 дней.</span><br />
                            Даже если есть аресты, обременения, торги, ипотека, залоги.
                        </h2>
                    </div>

                    <div class="hero__text-holder">
                        <div class="timeline-header">Процедура выкупа</div>
                        @include('parts.index.time-line')
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="form_ransom">
                        @include('parts.index_form')
                    </div>
                </div>

            </div>

            {{--@include('parts.index_form')--}}

        </div>
    </section> <!-- end hero -->

    <div id="sale_block">
        @include('parts.index.sales')
    </div>

    <section class="section-wrap" style="margin-bottom: 0; padding-bottom: 0" id="purchase_block">
        <div class="container">
            <div class="title-row title-row--boxed text-center">
                <h2 class="section-title">Покупка квартиры</h2>
                <p class="subtitle">Ниже рынка от 3 до 10%</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6 offset-lg-1">
                @include('parts.index.time-line')
            </div>
            <div class="col-lg-5">
                <div class="form_ransom">
                    @include('parts.index_form')
                </div>
            </div>
        </div>
    </section>
    {{--@include('parts.index.news', ['moreItems' => $moreItems])--}}

    <!-- Partners -->
    {{--@include('parts.index.map_objects')--}}
    <!-- end partners -->

    <!-- CTA -->
    {{--<div class="container offset-top-152 pt-sm-48">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="call-to-action box-shadow-large text-center">
                    <div class="call-to-action__container">
                        <h3 class="call-to-action__title">
                            Какой-то мотивирующий заголовок
                        </h3>
                        <a href="#" class="btn btn--lg btn--color">
                            <span>Давайте работать вместе!</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end cta -->--}}
@endsection
@push('svg_main_img')
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 630 480" style="enable-background:new 0 0 600 480;" xml:space="preserve" class="triangle-img triangle-img--align-right">
      <g>
          <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#img1)" />
          <path class="st0" d="M232.16,108.54,76.5,357.6C43.2,410.88,81.5,480,144.34,480H455.66c62.83,0,101.14-69.12,67.84-122.4L367.84,108.54C336.51,58.41,263.49,58.41,232.16,108.54Z" fill="url(#triangle-gradient)" fill-opacity="0.7" />
      </g>
        <defs>
            <pattern id="img1" patternUnits="userSpaceOnUse" width="500" height="500">
                <image xlink:href="img/10533952.png" x="50" y="70" width="500" height="500"></image>
            </pattern>

            <linearGradient id="triangle-gradient" y2="100%" x2="0" y1="50%" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#4C86E7" />
                <stop offset="1" stop-color="#B939E5" />
            </linearGradient>
        </defs>
    </svg>
@endpush
