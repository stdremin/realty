@extends('layouts.inner_content')
@section('inner_title')
    @component('components.title');
    @slot('title'){{ $title }}@endslot
    @slot('subTitle'){{ $subTitle }}@endslot
    @endcomponent
@stop
@section('inner_text')
    <!-- Contact -->
    <section class="section-wrap">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="contact box-shadow-large offset-top-171">
                        <ul class="contact__items">
                            <li class="contact__item">@include('parts.address')</li>
                            <li class="contact__item">@include('parts.phone')</li>
                            <li class="contact__item">@include('parts.email')</li>
                        </ul>

                        <!-- Contact Form -->
                        <form id="contact-form" class="contact-form mt-30 mb-30" method="post" action="#">
                            <div class="row row-30">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="name">Имя <abbr title="required" class="required">*</abbr></label>
                                        <input name="name" id="name" type="text">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label for="email">Email <abbr title="required" class="required">*</abbr></label>
                                        <input name="email" id="email" type="email">
                                    </div>
                                </div>
                            </div>

                            <div class="contact-message">
                                <label for="message">Сообщение <abbr title="required" class="required">*</abbr></label>
                                <textarea id="message" name="message" rows="7" required="required"></textarea>
                            </div>

                            <input type="submit" class="btn btn--lg btn--color btn--wide btn--button" value="Отправить" id="submit-message">
                            <div id="msg" class="message"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- end contact -->

    <!-- Google Map -->

    <div id="google-map" class="gmap"></div>

@stop
@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?apikey=5f06fd06-2719-4da0-9844-e1a8127e458e&lang=ru_RU" type="text/javascript"></script>
    <script>
        $(function () {
	        ymaps.ready(function () {
		        var myMap = new ymaps.Map("google-map", {
			        // Координаты центра карты.
			        // Порядок по умолчанию: «широта, долгота».
			        // Чтобы не определять координаты центра карты вручную,
			        // воспользуйтесь инструментом Определение координат.
			        center: [55.76, 37.64],
			        // Уровень масштабирования. Допустимые значения:
			        // от 0 (весь мир) до 19.
			        zoom: 9
		        });
	        });

        });
    </script>
@endpush