@extends('layouts.inner_content')
@section('inner_title')
    @component('components.title');
        @slot('title'){{ $title }}@endslot
        @slot('subTitle'){{ $subTitle }}@endslot
    @endcomponent
@stop
@section('inner_text')
    <!-- Blog -->
    <?/** @var \App\Models\ArticlesModel $post */?>
    <section class="section-wrap bottom-divider">
        <div class="container">
            <div class="row card-row">
                @foreach($items as $post)
                    <div class="col-lg-4">
                        <article class="entry card box-shadow hover-up">
                            <div class="entry__img-holder card__img-holder">
                                <a href="{{ route('detail_article', ['code' => $post['code']]) }}">
                                    <img src="{{ $post->preview_image }}" class="entry__img" alt="{{ $post->title }}">
                                </a>
                                <div class="entry__date">
                                    <span class="entry__date-day">27</span>
                                    <span class="entry__date-month">aug</span>
                                </div>
                                <div class="entry__body card__body">
                                    <h4 class="entry__title">
                                        <a href="{{ route('detail_article', ['code' => $post['code']]) }}">{{ $post->title }}</a>
                                    </h4>
                                    <ul class="entry__meta">
                                        <li class="entry__meta-category">
                                            <i class="ui-category"></i>
                                            <a href="#">Marketing</a>
                                        </li>
                                        <li class="entry__meta-comments">
                                            <i class="ui-comments"></i>
                                            <a href="#">3 Comments</a>
                                        </li>
                                    </ul>
                                    <div class="entry__excerpt">
                                       {!! $post->preview_text !!}
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
            <!-- Pagination -->
            {{ $items->links('vendor.pagination.default') }}

        </div>
    </section>
    <!-- end blog -->
@endsection