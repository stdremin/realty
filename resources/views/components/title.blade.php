<!-- Page Title -->
<section class="page-title text-center">
    <div class="container">
        <div class="page-title__holder">
            <h1 class="page-title__title">{{ $title }}</h1>
            <p class="page-title__subtitle">{{ $subTitle }}</p>
        </div>
    </div>
</section>
<!-- end page title -->