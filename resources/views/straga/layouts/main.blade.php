<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Админка</title>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,400i,500,700' rel='stylesheet'>


</head>
<body data-menu="vertical-menu" class="vertical-layout vertical-menu 2-columns menu-expanded">
    <nav role="navigation" class="header-navbar navbar-expand-sm navbar navbar-with-menu fixed-top navbar-dark navbar-shadow navbar-border">
    </nav>

    <!-- BEGIN Navigation-->
    <div class="main-menu menu-dark menu-fixed menu-shadow">
    </div>
    <!-- END Navigation-->
    <!-- BEGIN Content-->
    <div class="content app-content">
    </div>
    <!-- END Content-->
    <!-- START FOOTER DARK-->
    <footer class="footer footer-dark">
    </footer>
    <!-- START FOOTER DARK-->
</body>
</html>
