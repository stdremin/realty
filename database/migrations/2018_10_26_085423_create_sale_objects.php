<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_objects', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('title', 255);
	        $table->string('code', 255)->nullable()->index();
	        $table->string('preview_image')->nullable();
	        $table->string('detail_image')->nullable();
	        $table->text('preview_text')->nullable();
	        $table->text('detail_text')->nullable();
	        $table->integer('map_point_id')->nullable()->index();
	        $table->integer('section_id')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_objects');
    }
}
