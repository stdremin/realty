<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@indexPage');

Route::get('/about',  function (){
	$data = [
		'title' => 'О нас',
		'subTitle' => 'Соединение параллельно. Эпоха, несмотря на внешние воздействия, иллюстрирует вращательный Южный Треугольник,
		 Плутон не входит в эту классификацию. Различное расположение вызывает близкий сарос.'
	];
	return view('pages.about', $data);
});

Route::get('/contacts',  function (){
	$data = [
		'title' => 'Контактная информация',
		'subTitle' => 'Есть вопросы? Задайте их в этой форме.'
	];
	return view('pages.contact', $data);
});

Route::get('/articles', 'Articles@index');
Route::get('/articles/{code}', 'Articles@getDetail')->name('detail_article');
Route::get('/deals', 'Articles@deals')->name('deals');
